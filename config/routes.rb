Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'sessions#new'

  # Sessions routes
  get 'logout', :to => 'sessions#destroy'
  resources :sessions

  # Passwords routes
  get 'passworrds/new'
  get 'passworrds/forgot'
  get 'passworrds/new_reset'
  get 'passworrds/reset'

  # Users routes
  resources :users do

    member do
      get :delete
    end

    member do
      get :show_answers
    end

    collection do
      get :about_developers
    end

    collection do
      get :show_users
    end

    collection do
      get :search
    end

    collection do
      get :search_user
    end

  end

  # Subjects routes
  resources :subjects do

    member do
      get :delete
    end

    member do
      get :subject_questions
    end

    member do
      get :unanswered
    end

    member do
      get :answered
    end

    collection do
      get :search
    end

    collection do
      get :search_subject
    end

    member do
      get :delete_question
    end

  end

  # Questions routes
  resources :questions do

    collection do
      get :unanswered
    end

    collection do
      get :answered
    end

    collection do
      get :search
    end

    member do
      get :delete
    end

    member do
      get :delete_question
    end

    member do
      get :d
    end

  end

  # Answers routes
  resources :answers do

    member do
      get :delete
    end

    member do
      get :rank_answer
    end

    member do
      get :create_comment
    end

    member do
      get :new_comment
    end

  end


end
