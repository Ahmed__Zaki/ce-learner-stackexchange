class JsonWebToken

    HMAC_SECRET = 'my$ecretK3y'
  
    ALGORITHM   = 'HS256'.freeze
    
  
    def self.encode(payload: {}, exp: 720.hours.from_now)
      payload[:iat] = Time.now.to_i
      payload[:exp] = exp.to_i
  
      JWT.encode(payload, HMAC_SECRET, ALGORITHM)
    end

    # [{"user_id"=>1, "iat"=>"2018-12-31 17:31:19 UTC"}, {"alg"=>"HS256"}]
     def self.valid? token
        begin
            # HashWithIndifferentAccess.new(JWT.decode(token, HMAC_SECRET))
            JWT.decode(token, HMAC_SECRET)
        rescue
            false
        end
    end
  
end