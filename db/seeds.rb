# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# u1 = User.create( { username: Faker::Name, password: '123456789', email: Faker::Internet.email, avatar: Faker::Avatar.image } )

# s1 = Subject.create( { name: ""} )
# s2 = Subject.create( { name: "C#"} )
# s3 = Subject.create( { name: "OOP"} )
# q1 = Question.create( { title: "definition of java" ,question: "What is meaning java and what is the basics of it", user_id: 4, subject_id: 1} )
# q1 = Question.create( { title: "definition of c#" , question: "What is meaning c# and what is the basics of it", user_id: 3, subject_id: 1} )
# a1 = Answer.create( { answer: "q++ is programming language", user_id: 1, question_id: 3} )

