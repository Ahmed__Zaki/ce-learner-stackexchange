class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|

      t.string :username
      t.string :email
      t.string :password_digest
      t.integer :role
      t.integer :status
      t.integer :questions_count, default: 0
      t.integer :answers_count, default: 0

      t.timestamps
    end
  end
end
