class CreateSubjects < ActiveRecord::Migration[5.2]
  def change
    create_table :subjects do |t|

      t.string :name
      t.integer :questions_count, default: 0

      t.timestamps
    end
  end
end
