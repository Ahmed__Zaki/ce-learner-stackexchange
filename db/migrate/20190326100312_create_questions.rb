class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|

      t.text :title
      t.text :question
      t.references :user, foreign_key: true
      t.references :subject, foreign_key: true
      t.integer :answers_count, default: 0
      t.integer :views_count, default: 0

      t.timestamps
    end
  end
end
