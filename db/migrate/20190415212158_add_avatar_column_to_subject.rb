class AddAvatarColumnToSubject < ActiveRecord::Migration[5.2]
  
  def up
    add_attachment :subjects, :avatar
  end

  def down
    remove_attachment :subjects, :avatar
  end

end
