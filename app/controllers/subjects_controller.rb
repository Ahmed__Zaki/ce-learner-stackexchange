class SubjectsController < ApplicationController

  layout 'admin'

  before_action :set_subject, only: [:subject_questions, :unanswered, :answered, :edit, :update, :delete]

  def index
    @subjects = Subject.all
  end

  def subject_questions
    session[:subject_id] = params[:id]
    @questions = @subject.questions.newest
  end

  def unanswered
    @questions = @subject.questions.unanswered
    render('subject_questions')
  end

  def answered
    @questions = @subject.questions.answered
    render('subject_questions')
  end

  def search
    @subject = Subject.find(session[:subject_id])
    @questions = @subject.questions.ransack( title_cont: params[:search] ).result
    render('subject_questions')
  end

  def search_subject
    @subjects = Subject.ransack( name_cont: params[:search] ).result
    render('show')
  end

  def show
    @subjects = Subject.all
  end

  def new
    @subject = Subject.new
  end

  def create
    @subject = Subject.new( subject_params )
    if @subject.save
      flash[:create_subject] = nil
      redirect_to(subject_path(1))
    else
      flash[:create_subject] = @subject.errors.full_messages
      render('new')
    end
  end

  def edit
  end

  def update
    if @subject.update_attributes( subject_params )
      redirect_to( subject_path(1) )
    else
      render('edit')
    end
  end

  def delete
    @subject.destroy
    redirect_to( subject_path(1) )
  end

  def delete_question
    @question = Question.find(params[:id])
    @question.destroy
    redirect_to(subject_questions_subject_path(session[:subject_id]))
  end

  def destroy
  end


  private

  def subject_params
    params.require(:subject).permit( :name, :avatar )
  end

  def set_subject
    @subject = Subject.find(params[:id])
  end

end
