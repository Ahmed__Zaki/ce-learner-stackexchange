class QuestionsController < ApplicationController

  layout 'admin'

  def index
    # @questions = Question.ransack( title_cont: "w" ).result
    @questions = Question.newest
    flash[:search] = params[:search]
  end

  def unanswered
    @questions = Question.unanswered
    render('index')
  end

  def answered
    @questions = Question.answered
    render('index')
  end

  def search
    @questions = Question.ransack( title_cont: params[:search] ).result
    render('index')
  end

  def show
    @question = Question.find( params[:id] )
    @question.set_views
    session[:question_id] = @question.id
  end

  def new
    @question = Question.new
  end

  def create
    @question = Question.new
    # flash[:subject_id] = params[:question][:subject_id]
    # return render('new')
    @question = Question.new( question_params )

    if @question.save
      flash[:create_question] = nil
      redirect_to(questions_path)
    else
      flash[:create_question] = @question.errors.full_messages
      render('new')
    end
  end

  def d
    @question = Question.new
    flash[:subject_id] = params[:id]
    # if @subject
      # redirect_to(questions_path)
    # else
      render('new')
    # end
  end

  def edit
    @question = Question.find( params[:id] )
  end

  def update
    @question = Question.find(params[:id])

    if @question.update_attributes(question_params)
      redirect_to(question_path(@question))
    else
      render('edit')
    end
  end

  def delete
    @question = Question.find(params[:id])
    @question.destroy
    redirect_to(user_path(@current_user))
  end

  def delete_question
    @question = Question.find(params[:id])
    @question.destroy
    redirect_to(questions_path)
  end

  def destroy
    
  end


  private

  def question_params
    params[:question][:user_id] = @current_user.id
    params.require(:question).permit( :title ,:question, :subject_id, :user_id )
  end

end
