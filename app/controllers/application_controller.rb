class ApplicationController < ActionController::Base

    load 'lib/JsonWebToken.rb'

    before_action :set_authentication
    attr_accessor :current_user

    private 

    def set_authentication
        token = session[:token]
        token_decod = JsonWebToken.valid? token

        if token_decod
            @current_user = User.find_by_id token_decod[0]["id"]
        else
            redirect_to(new_session_path)
        end
    end

end
