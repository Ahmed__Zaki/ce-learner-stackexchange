class PassworrdsController < ApplicationController

  layout 'application'

  skip_before_action :set_authentication

  def new
  end

  def forgot
    if params[:email].blank? # check if email is present
      # flash[:forgot_password] = "Enter email"
      return render('new')
    end

    user = User.find_by(email: params[:email]) # if present find user by email

    if user.present?
      flash[:forgot_password] = nil
      user.generate_password_token! #generate pass token
      UserMailer.forgot_password(user).deliver_now    # SEND EMAIL HERE
      return redirect_to(passworrds_new_reset_path)
    else
      flash[:forgot_password] = "Invalid email"
      return render('new')
    end
  end

  def new_reset
  end

  def reset
    token = params[:token].to_s

    if params[:token].blank?
      flash[:forgot_password] = "Enter code"
      return render('new_reset')
    end

    user = User.find_by(reset_password_token: token)

    if user.present? && user.password_token_valid?
        if user.reset_password!(params[:password])
          flash[:forgot_password] = "Success change password"
          return redirect_to(new_session_path)
        else
          flash[:forgot_password] = user.errors.full_messages
          render('new_reset')   
        end
    else
      flash[:forgot_password] = "Invalid (Confirm from code or sen it again)"
    end
    return render('new_reset')
  end

  
end
