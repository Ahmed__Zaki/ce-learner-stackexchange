class UsersController < ApplicationController

  layout 'admin'

  before_action :set_user, only: [ :show, :show_answers, :edit, :update, :delete ]

  def index
    @users = User.newest
  end

  def show_users
    @users = User.newest
  end

  def show
    @questions = @user.questions
    @show_delete_update = true

  end

  def show_answers
    @answers = @user.answers
    @questions = @answers.map {|a| a.question }.uniq
    @show_delete_update = false
    render('show')
  end

  def search
    @user = User.find( @current_user.id )
    @questions = @user.questions
    @questions = @questions.ransack( title_cont: params[:search] ).result
    @show_delete_update = true
    render('show')
  end

  def search_user
    @users = User.ransack( username_cont: params[:search] ).result
    render('show_users')
  end

  def new
    @user = User.new
  end

  def create
    # Instantiate a new object using form parameters
    @user = User.new( user_params )
    # Save the object
    if @user.save
    # If save succeeds, redirect to the index action
      redirect_to(show_users_users_path)
    else
    # If save fails, redisplay the form so user can fix problems
      flash[:create_user] = @user.errors.full_messages
      render('new')
    end
  end

  def edit
  end

  def update
    if @user.update_attributes(user_params)
      redirect_to(show_users_users_path)
    else
      render('edit')
    end
  end

  def delete
    @user.destroy
    redirect_to(show_users_users_path)
  end

  def about_developers
  end

  private

  def user_params
    params.require(:user).permit( :username , :password, :email, :role, :status, :avatar )
  end

  def set_user
    @user = User.find( params[:id] )
  end

end
