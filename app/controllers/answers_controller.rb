class AnswersController < ApplicationController

    layout 'admin'

    def create
        @answer = Answer.new( answer_param )
        
        if @answer.save
            flash[:create_answer] = nil
            redirect_to( question_path(@answer.question) )
        else
            flash[:create_answer] = @answer.errors.full_messages
            redirect_to( question_path(session[:question_id]) )
        end
    end

    def edit
        @answer = Answer.find( params[:id] )
    end

    def update
        @answer = Answer.find(params[:id])

        if @answer.update_attributes(answer_params)
            redirect_to( question_path(@answer.question) )
        else
            render('edit')
        end
    end

    def delete
        @answer = Answer.find(params[:id])
        @answer.destroy
        redirect_to( question_path(@answer.question) )
    end

    def destroy
    
    end

    def rank_answer
        @answer = Answer.find( params[:id] )
        @answer.set_ranke
        redirect_to ( question_path(@answer.question) )
    end

    def new_comment
        @answer = Answer.find( params[:id] )
        @comments = @answer.comments.reverse
        session[:answer_id] = @answer.id
    end

    def create_comment
        @comment = Comment.new( comment_params )
        
        if @comment.save
            redirect_to( new_comment_answer_path(session[:answer_id]) )
        else
            redirect_to( question_path(session[:question_id]) )
        end
    end

    private

    def answer_params
        params[:answer][:user_id] = @current_user.id
        params[:answer][:question_id] = session[:question_id] 
        params.require(:answer).permit( :answer ,:question_id, :user_id )
    end

    def answer_param
        params[:user_id] = @current_user.id
        params[:question_id] = session[:question_id] 
        params.permit( :answer ,:question_id, :user_id )
    end

    def comment_params
        params[:user_id] = @current_user.id
        params[:answer_id] = params[:id]
        params.permit(:comment, :user_id, :answer_id)
    end

end
