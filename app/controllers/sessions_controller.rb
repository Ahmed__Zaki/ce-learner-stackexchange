class SessionsController < ApplicationController

  layout 'application'

  skip_before_action :set_authentication

  def new
  end

  def create

    if params[:username].present? && params[:password].present?
      user = User.login(params[:username], params[:password])

      if user
        session[:token] = user
        return redirect_to(questions_path)
      else
        flash[:notice] = "Invalid username/password combination."  
        return render('new')
      end
    else
      flash[:notice] = "Fill Field"
      return render('new')
    end

  end

  def destroy
    session[:token] = nil
    flash[:logout] = "Success logout"
    render('new')
  end

end
