# == Schema Information
#
# Table name: users
#
#  id                     :bigint(8)        not null, primary key
#  username               :string
#  email                  :string
#  password_digest        :string
#  role                   :integer
#  status                 :integer
#  questions_count        :integer          default(0)
#  answers_count          :integer          default(0)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  avatar_file_name       :string
#  avatar_content_type    :string
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#

class User < ApplicationRecord

    ##-----------------------------Reqiurments--------------------##
    load 'lib/JsonWebToken.rb'
    extend Enumerize
    has_secure_password
    # Ex:- scope :active, -> {where(:active => true)}
    scope :newest, -> { order( "created_at DESC" ) }
    scope :active, -> { where( status: "active" ) }

    ##--------------------------Assocciations-------------------##
    has_many :comments, dependent: :destroy
    has_many :questions, dependent: :destroy
    has_many :answers, dependent: :destroy


    ##-----------------------------Callbacks--------------------##
    before_validation :set_email_dwoncase
   
    
    ##-----------------------------Validations--------------------##
    has_attached_file :avatar , styles: { medium: "300x300>", thumb: "40x40>" }
    validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
    validates :avatar, presence: true, on: :create

    validates :username, presence: true,   
                            uniqueness: true
                            # length: {minimum:10, maximum:50}

    EMAIL_REGEX = /\A[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\Z/i
    validates :email, presence: true,
                        uniqueness: true,
                        format: EMAIL_REGEX

    # validates :password, presence: true,   
                            # length: {minimum:9, maximum:20}


    ##-----------------------------Enums------------------------##
    #Enumerize Status
    enumerize :status, in: {
        active: 1,
        unactive: 2
    },default: 1, scope: true, predicates: true

    #Enumerize Role
    enumerize :role, in: {
        admin: 1,
        instructor: 2,
        student: 3
    },default: 3, scope: true, predicates: true


    ##-----------------------------Class methods------------------##

    def self.login login, password 
        user = active.find_by( "username = :login OR email = :login", {login: login} )

        if user && user.authenticate( password )
            user.auth_token
        else
            false
        end
    end

    def generate_password_token!
        self.reset_password_token = generate_token
        self.reset_password_sent_at = Time.now.utc
        self.password = "123456789"
        save!
    end
        
    def password_token_valid?
        (self.reset_password_sent_at + 4.hours) > Time.now.utc
    end
    
    def reset_password!(password)
        self.reset_password_token = nil
        self.password = password
        save!
    end
    

    ##-----------------------------Methods------------------------##

    def login_payload  
        {  
            id:    id,   email: email 
        }
    end


    def auth_token
        JsonWebToken.encode( payload: login_payload )
    end


    private

    def set_email_dwoncase
        self.email = email.downcase if self.email.present?
    end

    def generate_token
        SecureRandom.hex(10)
    end

end
