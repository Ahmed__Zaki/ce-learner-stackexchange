# == Schema Information
#
# Table name: answers
#
#  id          :bigint(8)        not null, primary key
#  answer      :text
#  rank        :boolean          default(FALSE)
#  user_id     :bigint(8)
#  question_id :bigint(8)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Answer < ApplicationRecord

    ##-----------------------------Reqiurments--------------------##
    scope :rank_answer, -> { where( rank: true ) }

    ##--------------------------Assocciations-------------------##
    has_many :comments, dependent: :destroy
    belongs_to :user, counter_cache: true
    belongs_to :question, counter_cache: true


    ##-----------------------------Callbacks--------------------##

    ##-----------------------------Validations--------------------##
    validates :answer, presence: true


    ##-----------------------------Enums------------------------##

    ##-----------------------------Class methods------------------##
    ##-----------------------------Methods------------------------##

    def set_ranke
        self.rank ? self.rank = false : self.rank = true
        save!
    end

end
