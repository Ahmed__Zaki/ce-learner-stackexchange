# == Schema Information
#
# Table name: subjects
#
#  id                  :bigint(8)        not null, primary key
#  name                :string
#  questions_count     :integer          default(0)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  avatar_file_name    :string
#  avatar_content_type :string
#  avatar_file_size    :integer
#  avatar_updated_at   :datetime
#

class Subject < ApplicationRecord

    ##-----------------------------Reqiurments--------------------##

    ##--------------------------Assocciations-------------------##
    has_many :questions, dependent: :destroy


    ##-----------------------------Callbacks--------------------##

    ##-----------------------------Validations--------------------##
    has_attached_file :avatar , styles: { medium: "300x300>", thumb: "40x40>" }
    validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

    validates :name, presence: true,
                        uniqueness: true


    ##-----------------------------Enums
    ##-----------------------------Class methods------------------##
    ##-----------------------------Methods------------------------##

    def sub
        2 - 5
    end


end
