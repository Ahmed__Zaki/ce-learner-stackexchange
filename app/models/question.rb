# == Schema Information
#
# Table name: questions
#
#  id            :bigint(8)        not null, primary key
#  title         :text
#  question      :text
#  user_id       :bigint(8)
#  subject_id    :bigint(8)
#  answers_count :integer          default(0)
#  views_count   :integer          default(0)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Question < ApplicationRecord

    ##-----------------------------Reqiurments--------------------##
    # Ex:- scope :active, -> {where(:active => true)}
    scope :newest, -> { order( "created_at DESC" ) }
    scope :unanswered, -> { where( answers_count: 0 ) }
    scope :answered, -> { where( "answers_count > 0" ) }
    


    ##--------------------------Assocciations-------------------##
    has_many :answers, dependent: :destroy
    belongs_to :user, counter_cache: true
    belongs_to :subject, counter_cache: true


    ##-----------------------------Callbacks--------------------##

    ##-----------------------------Validations--------------------##
    validates :title, presence: true

    validates :question, presence: true,   
                            uniqueness: true
    
    
    ##-----------------------------Enums-------------------------##
    ##-----------------------------Class methods------------------##

    ##-----------------------------Methods------------------------##
    
    def set_views
        self.views_count += 1
        save!
    end


end
