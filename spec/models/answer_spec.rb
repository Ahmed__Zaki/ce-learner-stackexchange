# == Schema Information
#
# Table name: answers
#
#  id          :bigint(8)        not null, primary key
#  answer      :text
#  rank        :boolean          default(FALSE)
#  user_id     :bigint(8)
#  question_id :bigint(8)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Answer, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"

  before(:all) do
    @answer = create(:answer)
  end

  describe "Validation" do
    it { should validate_presence_of(:answer) }
  end

  describe "Methods" do

    # let(:user) { User.new(username: "aymen1 zaki bashkeel", password: "123456789",
    #     email: "ayMen1@gmail.com", role: "admin" ,status: "active") }
      
    # let(:subject) { Subject.new(name: "OOP") }

    # let(:question) { Question.new(title: "OOP", question: "What is OOP", 
    # user_id: user.id, subject_id: subject.id) }

    # let(:answer) {
    #   described_class.new(answer: "OOP is difficult programming language" ,user_id: user.id, question_id: question.id)
    # }
  
    it "is valid with valid attributes" do
      # save_user_subject_question
      expect(@answer).to be_valid
    end

    it "#set_ranke" do 
      # save_user_subject_question
      @answer.set_ranke
      expect(@answer.rank).to eq(true)
      
      @answer.set_ranke
      expect(@answer.rank).to eq(false)
    end
  
  end

  describe "Assocciation" do
    
    it { should belong_to(:user).counter_cache(true) }
    it { should belong_to(:question).counter_cache(true) }
  
  end

end



