# == Schema Information
#
# Table name: subjects
#
#  id                  :bigint(8)        not null, primary key
#  name                :string
#  questions_count     :integer          default(0)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  avatar_file_name    :string
#  avatar_content_type :string
#  avatar_file_size    :integer
#  avatar_updated_at   :datetime
#

require 'rails_helper'

RSpec.describe Subject, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"

  before(:all) do
    @subject = create(:subject)
  end

  it "is valid with valid attributes" do
    expect(@subject).to be_valid
  end

  describe "Validations" do
      
    context "name" do
      it { should validate_presence_of(:name) }
      it { should validate_uniqueness_of(:name) }
    end

  end
  
  describe "Assocciation" do
    
    it { should have_many(:questions) }
  
  end

end

