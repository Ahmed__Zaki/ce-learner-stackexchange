# == Schema Information
#
# Table name: users
#
#  id                     :bigint(8)        not null, primary key
#  username               :string
#  email                  :string
#  password_digest        :string
#  role                   :integer
#  status                 :integer
#  questions_count        :integer          default(0)
#  answers_count          :integer          default(0)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  avatar_file_name       :string
#  avatar_content_type    :string
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#

require 'rails_helper'

RSpec.describe User, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"

  before(:all) do
    @user = create(:user)
  end

  describe "Validations" do
       
    it "is valid with valid attributes" do
      expect(@user).to be_valid
    end
  
    context "username" do

      it { should validate_presence_of(:username) }
      it { should validate_uniqueness_of(:username) }
      # it { should validate_length_of(:username).is_at_least(10).is_at_most(50) }

    end

    context "email" do

      it { should validate_presence_of(:email) }
      # it { should validate_uniqueness_of(:email) }
      # it { should allow_value('ahmed@gmail.com').for(:email) }

    end
  
    context "password" do

      it { should have_secure_password }
    
    end

    context "Role" do
      
      # it do
      #   should define_enum_for(:role).
      #     with_values(
      #       admin: 1,
      #       instructor: 2,
      #       student: 3
      #     ).backed_by_column_of_type(:integer)
      # end

    end

    context "status" do
      
      # it do
      #   should define_enum_for(:status).
      #     with_values(
      #       active: 1,
      #       inactive: 2
      #     ).backed_by_column_of_type(:integer)
      # end

    end

  end
  
  describe "Callbacks" do

    it "before validation set_email_dwoncase" do
      user1 = create(:user, email: 'ahmEd@gmail.cOm')
      expect(user1.email).to eq("ahmed@gmail.com")
    end

  end

  describe "Assocciation" do

    it { should have_many(:questions) }
    it { should have_many(:answers) }

  end

end
