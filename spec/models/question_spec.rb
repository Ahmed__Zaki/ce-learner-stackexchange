# == Schema Information
#
# Table name: questions
#
#  id            :bigint(8)        not null, primary key
#  title         :text
#  question      :text
#  user_id       :bigint(8)
#  subject_id    :bigint(8)
#  answers_count :integer          default(0)
#  views_count   :integer          default(0)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe Question, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"

  before(:all) do
    @question = create(:question)
  end

#   let(:user) { User.new(username: "aymen1 zaki bashkeel", password: "123456789",
#     email: "ayMen1@gmail.com", role: "admin" ,status: "active") }
  
#   let(:subject) { Subject.new(name: "OOP") }

#   let(:question) { Question.new(title: "OOP", question: "What is OOP", 
#     user_id: user.id, subject_id: subject.id) }


  describe "Validations" do

    it "is valid with valid attributes" do
      expect(@question).to be_valid
    end
      
    it { should validate_presence_of(:title) }

    it { should validate_presence_of(:question) }
  
  end

  describe "Assocciation" do
    
    it { should have_many(:answers).dependent(:destroy) }
    it { should belong_to(:user).counter_cache(true) }
    it { should belong_to(:subject).counter_cache(true) }
  
  end

  describe "#Instance methode" do
    
    it "#set_views" do
      @question.set_views
      expect(@question.views_count).to eq(1)

      @question.set_views
      expect(@question.views_count).to eq(2)
    end
  
  end

end


