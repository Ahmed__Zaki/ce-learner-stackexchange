# == Schema Information
#
# Table name: questions
#
#  id            :bigint(8)        not null, primary key
#  title         :text
#  question      :text
#  user_id       :bigint(8)
#  subject_id    :bigint(8)
#  answers_count :integer          default(0)
#  views_count   :integer          default(0)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryBot.define do

  # factory :user 
  # factory :subject

  factory :question do
    title { 'OOP' }
    question { 'What is OOP' }
    user
    subject
  end
end
