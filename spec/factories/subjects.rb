# == Schema Information
#
# Table name: subjects
#
#  id                  :bigint(8)        not null, primary key
#  name                :string
#  questions_count     :integer          default(0)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  avatar_file_name    :string
#  avatar_content_type :string
#  avatar_file_size    :integer
#  avatar_updated_at   :datetime
#

FactoryBot.define do
  factory :subject do
    name {'OOP'}
  end
end
