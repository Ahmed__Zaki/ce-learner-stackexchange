# == Schema Information
#
# Table name: answers
#
#  id          :bigint(8)        not null, primary key
#  answer      :text
#  rank        :boolean          default(FALSE)
#  user_id     :bigint(8)
#  question_id :bigint(8)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :answer do

    answer { 'OOP is object oriented programming' }
    question
    user
    
  end
end
