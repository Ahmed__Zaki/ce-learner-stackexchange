# == Schema Information
#
# Table name: users
#
#  id                     :bigint(8)        not null, primary key
#  username               :string
#  email                  :string
#  password_digest        :string
#  role                   :integer
#  status                 :integer
#  questions_count        :integer          default(0)
#  answers_count          :integer          default(0)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  avatar_file_name       :string
#  avatar_content_type    :string
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#

FactoryBot.define do
  factory :user do
    avatar { Faker::Avatar.image }
    username { Faker::Internet.username }# 'ahmed zaki bashkeel'}
    email { Faker::Internet.email }# 'aHmED@gMail.cOm' }
    password { '123456789' }
  end
end
