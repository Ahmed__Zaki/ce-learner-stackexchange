require 'rails_helper'

RSpec.describe PassworrdsController, type: :controller do

  describe "GET #new_forget" do
    it "returns http success" do
      get :new_forget
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #new_reset" do
    it "returns http success" do
      get :new_reset
      expect(response).to have_http_status(:success)
    end
  end

end
